from typing import Dict, Union

import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from . import views
from .models import Grades

pytest_mark = pytest.mark.django_db


class GradeTests(APITestCase):
    def get_student(self, student_id: Union[int, str]) -> Dict:
        """
        Helper function that returns the response from retrieving a student
        :param student_id: primary key of the student
        :return: response from the `get` call
        """
        url = reverse(views.StudentDetail.name,
                      args=[student_id if isinstance(student_id, str) else str(student_id)]
                      )
        response = self.client.get(url, format='json')
        return response

    def post_student(self, first_name: str, last_name: str) -> Dict:
        """
        Helper function to add a new student
        :param first_name: student's first name
        :param last_name: student's last name
        :return: response from the post call
        """
        url = reverse(views.StudentList.name)
        data = {'first_name': first_name, 'last_name': last_name}
        response = self.client.post(url, data, format='json')
        return response

    def post_grade(self, class_name: str, grade_value: float, student_id: str) -> Dict:
        """
        Helper function to add a new grade
        :param class_name: name of the class
        :param grade_value: grade value
        :param student_id: primary key of related student
        :return: response from the post call
        """
        url = reverse(views.GradesList.name)
        data = {'student': student_id, 'class_name': class_name, 'grade_value': grade_value}
        response = self.client.post(url, data, format='json')
        return response

    def test_fail_save_grade_no_student(self):
        """
        Test that trying to save a grade without a related student fails
        GIVEN: just a grade without a student
        WHEN: post to the grades endpoint
        THEN: 400 status code is returned
        """
        response = self.post_grade('class', 93.2, None)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_save_grave_student(self):
        """
        Test that saving a grade with a student returns a 201 status
        GIVEN: just a grade with a student
        WHEN: post to the grades endpoint
        THEN: 201 status code is returned and there is one record in the Grades table
        """
        response_student = self.post_student('first', 'last')
        student_id = response_student.data['pk']
        response = self.post_grade('class', 93.2, student_id)
        assert response.status_code == status.HTTP_201_CREATED
        assert Grades.objects.count() == 1

    def test_save_two_grades(self):
        """
        Test that the students detail endpoint returns grades in the response
        GIVEN: a student with two grades
        WHEN: call the student detail endpoint for the student
        THEN: 2 items in the grades key of the response
        """
        response_student = self.post_student('first', 'last')
        student_id = response_student.data['pk']
        response_grade_1 = self.post_grade('class', 93.2, student_id)
        assert response_grade_1.status_code == status.HTTP_201_CREATED
        response_grade_2 = self.post_grade('other', 43.0, student_id)
        assert response_grade_2.status_code == status.HTTP_201_CREATED
        assert Grades.objects.count() == 2
        response_student_detail = self.get_student(student_id)
        assert len(response_student_detail.data['grades']) == 2
