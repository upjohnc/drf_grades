from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from student_grades import views

urlpatterns = [
    path("students/",
         views.StudentList.as_view(),
         name=views.StudentList.name),
    path('students/<int:pk>/',
         views.StudentDetail.as_view(),
         name=views.StudentDetail.name),
    path('grades/',
         views.GradesList.as_view(),
         name=views.GradesList.name),
    path('', views.ApiRoot.as_view()),
    path('grade_avg/<int:pk>/', views.grade_average)
]

urlpatterns = format_suffix_patterns(urlpatterns)
