from django.db.models import Avg
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from .models import Grades, Student
from .serializers import (GradeAverageSerializer, GradesSerializer,
                          StudentSerializer)


class StudentList(generics.ListCreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    name = 'student-list'


class StudentDetail(generics.RetrieveUpdateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    name = 'student-detail'


class GradesList(generics.ListCreateAPIView):
    queryset = Grades.objects.all()
    serializer_class = GradesSerializer
    name = 'grades-list'


class GradesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Grades.objects.all()
    serializer_class = GradesSerializer
    name = 'grades-detail'


@api_view(['GET'])
def grade_average(request, pk):
    instance = Grades.objects.filter(student=pk).aggregate(Avg('grade_value'))
    serializer = GradeAverageSerializer(instance)
    return Response(serializer.data)


class ApiRoot(generics.GenericAPIView):
    name = 'api-root'

    def get(self, request, *args, **kwargs):
        return Response({'grades': reverse(GradesList.name, request=request),
                         'student': reverse(StudentList.name, request=request),
                         }
                        )
