from rest_framework import serializers

from .models import Grades, Student


class GradesSerializer(serializers.ModelSerializer):
    student = serializers.SlugRelatedField(queryset=Student.objects.all(), slug_field='pk')

    class Meta:
        model = Grades
        fields = ('pk',
                  'class_name',
                  'grade_value',
                  'student',
                  )


class StudentSerializer(serializers.ModelSerializer):
    grades = GradesSerializer(many=True, read_only=True)

    class Meta:
        model = Student
        fields = ('pk',
                  'first_name',
                  'last_name',
                  'date_of_birth',
                  'grades',
                  )


class GradeAverageSerializer(serializers.BaseSerializer):
    def to_representation(self, instance):
        return {'grade_average': instance['grade_value__avg']}
