from django.apps import AppConfig


class StudentGradesConfig(AppConfig):
    name = 'student_grades'
