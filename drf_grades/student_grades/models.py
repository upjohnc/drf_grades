from django.db import models


class Student(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    date_of_birth = models.DateField(null=True, blank=True)

    class Meta:
        ordering = ['created']


class Grades(models.Model):
    student = models.ForeignKey(Student, related_name='grades', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    class_name = models.CharField(max_length=250)
    grade_value = models.FloatField()
