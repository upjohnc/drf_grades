# Django Rest Framework - Learning

This is my little project to get into DRF.
It is the application of the things learned in tutorials.<br>The project is the management of students and their grades.

## Step 1

- Add the model for students
- Add endpoint for list of students
- Add endpoint for single student

## Step 2

- Add model for grades
    - set student as a foreign key
- Added the grades serializer to the students serializer
- First attempt to add a grade through an http call resulted in an error that the student wasn't included
- Second attempt : realized that the model was forcing a student.  Decided to use a SlugRelatedField to be able to add the student id

## Step 3

- Add tests to confirm that grades are saving properly and that the student detail response includes grades

## Step 4

- Grade Average
    - To learn more deeply how a view and serializer interact, I set the next feature to return the average grade for a particular student.
    - Learned that I could set a simple function as a view with the `api_view` decorator
    - The query that I had used returned a dictionary rather than a class object, so I had to handle the dictionary in the serializer
    - Used just a basic serializer
